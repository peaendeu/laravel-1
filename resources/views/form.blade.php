<!-- Kab. Sukabumi, Muhammad Pandu Prasasti -->
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Buat Account Baru</title>
  </head>
  <body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="welcome" method="post">
      @csrf
      <label for="firstname">First name : </label>
      <br /><br />
      <input type="text" name="firstname" id="firstname" required autofocus autocomplete="off" />
      <br /><br />

      <label for="lastname">Last Name : </label>
      <br /><br />
      <input type="text" name="lastname" id="lastname" required autocomplete="off" />
      <br /><br />

      <label for="gender">Gender</label>
      <br /><br />
      <input type="radio" name="radio" id="radio1" />Male
      <br />
      <input type="radio" name="radio" id="radio2" />Female <br /><br />

      <label for="nationality">Nationality</label>
      <br /><br />
      <select name="nationality" id="nationality">
        <option value="indonesia">Indonesia</option>
        <option value="amerika">Amerika</option>
        <option value="arab">Arab</option>
      </select>
      <br /><br />

      <label for="language">Language Spoken</label>
      <br /><br />
      <input type="checkbox" name="" id="" />Bahasa Indonesia
      <br />
      <input type="checkbox" name="" id="" />English
      <br />
      <input type="checkbox" name="" id="" />Other <br /><br />

      <label for="bio">Bio</label>
      <br /><br />
      <textarea name="bio" id="bio" rows="5" cols="20"></textarea>
      <br />

      <button type="submit">Sign up</button>
    </form>
  </body>
</html>
